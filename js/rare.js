function isNumeric(value) {
    return /^-?\d+$/.test(value);
}

window.reapp = {
    app: "Rarefeitos 3.1.3",
    bgsound: "random",
    effects: [],
    tracks: [],
    alarms: [],
    alarmid: 0,
    duration: 30,
    volume: 50,
    change_title: true,
    actual: 0,
    _timer: false,
    _paused: false,
    playEffect: (eid) => {
        var audio = "soundboard/" + window.reapp.effects[eid].file
        $("#eftrack")[0].src = audio
        $("#eftrack")[0].play()
    },
    playBg: () => {
        $("#timer-play").hide()
        $("#timer-pause").show()
        if (window.reapp._paused) {
            $("#bgtrack")[0].play()
            window.reapp._paused = false
        } else {
            var bid = (window.reapp.bgsound == "random") ? Math.floor(Math.random() * window.reapp.tracks.length) : window.reapp.bgsound
            console.log(bid)
            var audio = "tracks/" + window.reapp.tracks[bid].file
            $("#bgtrack")[0].src = audio
            $("#bgtrack")[0].volume = window.reapp.volume / 100
            $("#bgtrack")[0].play()
            ons.notification.toast("<b>" + window.reapp.tracks[bid].name +
                '</b><br>Sound by ' + window.reapp.tracks[bid].author +
                ", under CC-0", { timeout: 3000, animation: 'ascend' })
        
        
            $("#app-title").text(window.reapp.tracks[bid].ptBr)
            $("#timer-stop").show()
            $("#timer").show()
        
            window.reapp.actual = 60 * window.reapp.duration
            window.reapp._paused = false
        
            window.reapp._timer = setInterval(() => {
                if (!window.reapp._paused) {
                    window.reapp.actual--;
                    var min = Math.floor(window.reapp.actual / 60)
                    var sec = window.reapp.actual % 60
                    if (sec < 10) {
                        sec = '0' + sec
                    }       
                    $("#timer").text(min + ':' + sec);
                
                    if (window.reapp.change_title) {
                        document.title = min + ":" + sec
                    }
                
                    if (window.reapp.actual <= 0) {
                        window.reapp.stopBg()
                        $("#altrack")[0].src = "alarms/" + window.reapp.alarms[window.reapp.alarmid].file
                        console.log(window.reapp.alarmid)
                        $("#altrack")[0].play()
                    }
                }
            }, 1000);
        }
    },
    pauseBg: () => {
        $("#timer-play").show()
        $("#timer-pause").hide()
        $("#bgtrack")[0].pause()        
        window.reapp._paused = true
    },
    stopBg: () => {
        $("#app-title").text(window.reapp.app)
        $("#timer-play").show()
        $("#timer-stop").hide()
        $("#timer-pause").hide()
        $("#bgtrack")[0].pause()
        $("#timer").hide()
        clearInterval(window.reapp._timer)
        document.title = window.reapp.app
    },
    updateSettingsTexts: () => {
        $.each($("ons-radio[name=track-radio]"), function (id, val) {
            if (window.reapp.bgsound == val.value) {
                val.checked = true
                $("#track-text").text(val.innerText)
            } else {
                val.checked = false
            }
        })
        $.each($("ons-radio[name=alarm-radio]"), function (id, val) {
            if (window.reapp.alarmid == val.value) {
                val.checked = true
                $("#alarm-text").text(val.innerText)
            } else {
                val.checked = false
            }
        })
    },
    loadSettings: () => {
        window.reapp.bgsound = ((localStorage.track > 0) || (localStorage.track == "random")) ? localStorage.track : 0;
        window.reapp.alarmid = (localStorage.alarm > 0) ? localStorage.alarm : 0;
        window.reapp.volume = (isNumeric(localStorage.volume)) ? localStorage.volume : 50;
        window.reapp.duration = (isNumeric(localStorage.pomo)) ? localStorage.pomo : 25;
        $("#volume-text").text(window.reapp.volume);
        $("#volume-range")[0].value = window.reapp.volume;
        $("#pomo-range")[0].value = window.reapp.duration;
        $("#pomo-text").text(window.reapp.duration);
        window.reapp.updateSettingsTexts();
        console.log(localStorage.volume + '=' + window.reapp.volume + '?');
    },
    settingsMenu: () => {
        $("#switch-title")[0].checked = window.reapp.change_title
        $("#volume-range")[0].value = window.reapp.volume
        $("#volume-text").text(window.reapp.volume)
        window.reapp.updateSettingsTexts()
        $("#pomo-range")[0].value = window.reapp.duration
        $("#pomo-text").text(window.reapp.duration)
        $("#settings")[0].show()
    },
    settingsApply: () => {
        localStorage.setItem('volume', window.reapp.volume);
        localStorage.setItem('pomo', window.reapp.duration);
        console.log(localStorage.volume);
        $("#settings")[0].hide();
    },
    start: () => {
        window.reapp.listEffects()
        window.reapp.listTracks()
        window.reapp.listAlarms()        
        document.title = window.reapp.app
        $("#timer-play").on("click", window.reapp.playBg)
        $("#timer-stop").on("click", window.reapp.stopBg)
        $("#timer-pause").on("click", window.reapp.pauseBg)
        $("#rare-settings").on("click", window.reapp.settingsMenu)
        $("#settings-button").on("click", window.reapp.settingsApply)
        
        $("#switch-title").on("change", function () {
            window.reapp.change_title = this.checked
            if (window.reapp.change_title || (window.reapp.actual > 0) ) {
                document.title = window.reapp.app
            }
        })
        
        $("#volume-range").on("change", () => {
            window.reapp.volume = $("#volume-range")[0].value
            $("#volume-text").text(window.reapp.volume)
            if (window.reapp.actual > 0) {
                $("#bgtrack")[0].volume = window.reapp.volume / 100
            }
        })
        $("#pomo-range").on("change", () => {
            window.reapp.duration = $("#pomo-range")[0].value
            $("#pomo-text").text(window.reapp.duration)
        })
        window.reapp.loadSettings();
    },
    listEffects: () => {
        $.getJSON('soundboard/list.json', (data) => {
            window.reapp.effects = data.list
        }).fail(function( jqxhr, textStatus, error ) {
            console.log( "Request Failed: " + textStatus + ', ' + error);
        }).then(() => {
            $.each(window.reapp.effects, function (ind, val) {
                var icocl = "md-play"
                switch (val.category) {
                    case "object":
                        icocl = "md-cutlery"
                        break
                    case "vocal":
                        icocl = "md-face"
                        break
                    case "nature":
                        icocl = "md-nature"
                        break
                    case "war":
                        icocl = "md-fire"
                        break
                    case "scifi":
                        icocl = "md-flare"
                        break
                    case "game":
                        icocl = "md-gamepad"
                        break
                    case "humor":
                        icocl = "md-cake"
                        break
                    case "music":
                        icocl = "md-album"
                        break
                }
                var leico = $("<ons-icon>", { icon: icocl, class: "list-item__icon" })
                var lelef = $("<div>", { class: "left" }).append(leico)
                var lecen = $("<div>", { class: "center" }).text(val.ptBr)
                var lelit = $("<ons-list-item>", { ripple: "true" }).append(lelef).append(lecen)
                    .on('click', () => { 
                        window.reapp.playEffect(ind)
                        ons.notification.toast("<b>" + val.name + '</b><br>Sound by ' + val.author + ", under CC-0", { timeout: 3000, animation: 'ascend', force: true })
                    })
                $("#effect-list").append(lelit)
            })
        })
    },
    clickOnTrack: function () {
        localStorage.setItem("track", this.value);
        window.reapp.loadSettings()
        $("#track-expan")[0].hideExpansion()
    },
    clickOnAlarm: function () {
        localStorage.setItem("alarm", this.value);
        window.reapp.loadSettings()
        $("#alarm-expan")[0].hideExpansion()
    },
    listTracks: () => {
        $("ons-radio[name=track-radio]").on("click", window.reapp.clickOnTrack)
        $.getJSON('tracks/list.json', (data) => {
            window.reapp.tracks = data.list
        }).fail(function( jqxhr, textStatus, error ) {
            console.log( "Request Failed: " + textStatus + ', ' + error);
        }).then(() => {
            $.each(window.reapp.tracks, function (ind, val) {
                var optio = $("<ons-radio>", { name: "track-radio", value: ind})
                    .text(val.ptBr).on("click", window.reapp.clickOnTrack)
                $("#track-sel").append(optio).append($("<br>"))
            })
        })
    },
    listAlarms: () => {
        $.getJSON('alarms/list.json', (data) => {
            window.reapp.alarms = data.list
        }).fail(function( jqxhr, textStatus, error ) {
            console.log( "Request Failed: " + textStatus + ', ' + error);
        }).then(() => {
            $.each(window.reapp.alarms, function (ind, val) {
                var optio = $("<ons-radio>", { name: "alarm-radio", value: ind})
                    .text(val.ptBr).on("click", window.reapp.clickOnAlarm)
                $("#alarm-sel").append(optio).append($("<br>"))
            })
        })
    },
}
